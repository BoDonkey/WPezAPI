<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Post\Taxonomies;

use WPezSuite\WPezAPI\Get\Post\Taxonomies\Term\ClassTerm;

class ClassTaxonomies {

    protected $_int_id;
    protected $_arr_taxs;
    protected $_arr_get_the_terms;


    public function __construct( $obj_post = false ) {

        if ( $obj_post !== false ){
            $this->setPostObject( $obj_post);
        }
    }

    protected function setPropertyDefaults(){

        $this->_int_id = false;
        $this->_arr_taxs = [];
        $this->_arr_get_the_terms = [];

    }


    // TOSO setPostType
    public function setPostObject( $obj = false ) {

        if ( $obj instanceof \WP_Post ) {

            $this->_int_id = $obj->ID;
            $this->_arr_taxs = get_object_taxonomies( $obj, 'objects' );

        }

    }

    public function __get( $str_key ) {

        $str_key = strtolower($str_key);

        if ( isset ( $this->_arr_taxs[ $str_key ] ) && $this->_arr_taxs[ $str_key ] instanceof \WP_Taxonomy) {

            if ( isset($this->_arr_taxs[ $str_key ]->terms)){
                return $this->_arr_taxs[ $str_key ];
            }

            $arr_gtt = get_the_terms( $this->_int_id, $str_key );
            $arr_terms = [];

            foreach ($arr_gtt as $obj_term ){

                $new = new ClassTerm($obj_term);
             //   $new->setTermObject($obj_term);
                $arr_terms[ strtolower($obj_term->slug ) ] = $new;
            }

            $this->_arr_get_the_terms[$str_key] = $arr_terms;


            $this->_arr_taxs[ $str_key ]->terms = $arr_terms;
            return $this->_arr_taxs[ $str_key ];

        }

        return [];

    }

}