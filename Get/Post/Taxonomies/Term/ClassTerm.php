<?php
/**
 * Created by PhpStorm.
 */


namespace WPezSuite\WPezAPI\Get\Post\Taxonomies\Term;

use WPezSuite\WPezAPI\Get\Meta\ClassMeta;


class ClassTerm {


    protected $_mix_ret;

    protected $_obj_term;
    protected $_int_id;

    protected $_link;
    protected $_obj_term_meta;


    public function __construct( $obj_term = false ) {

        $this->setPropertyDefaults();

        if ( $obj_term !== false ){
            $this->setTermObject( $obj_term );
        }
    }

    protected function setPropertyDefaults() {

        $this->_mix_ret = false;

        $this->_obj_term = false;
        $this->_int_id   = false;

        $this->_link          = false;
        $this->_obj_term_meta = false;
    }

    public function setTermObject( $obj_term = false ) {

        if ( $obj_term instanceof \WP_Term ) {

            $this->_obj_term = $obj_term;
            $this->_int_id   = $obj_term->term_id;

            return true;
        }

        return false;
    }


    public function __get( $str_prop ) {

        $str_prop = strtolower($str_prop);

        switch ( $str_prop ) {

            case 'id':
            case 'term_id':
           // case 'ID':
                return $this->getID();

            case 'name':
                return $this->getName();

            case 'slug':
                return $this->getSlug();

            case 'group':
            case 'term_group':

                return $this->getGroup();

            case 'taxonomy_id':
            case 'term_taxonomy_id':
                return $this->getTaxonomyID();

            case 'taxonomy':
            case 'taxonomy_name':
                return $this->getTaxonomy();

            case 'desc':
            case 'description':
                return $this->getDescription();

            case 'parent':
            case 'term_parent':
                return $this->getParent();

            case 'count':
                return $this->getCount();

            case 'filter':
                return $this->getFilter();

            case 'url':
            case 'link':
                return $this->getTermLink();

            case 'meta':
            // case 'Meta':
                return $this->getMeta();

            case 'next_id':
            case 'newer_id':
                return 'TODO'; // <<<<<<<<<<<<<<<<

            case 'prev_id':
            case 'older_id':
                return 'TODO'; // <<<<<<<<<<<<<<<<<

            default:
                return $this->_mix_ret;

        }

    }

    public function getID( $mix_fallback = null ) {

        return $this->getMaster( 'term_id', $mix_fallback );
    }


    public function getName( $mix_fallback = null ) {

        return $this->getMaster( 'name', $mix_fallback );
    }

    public function getSlug( $mix_fallback = null ) {

        return $this->getMaster( 'slug', $mix_fallback );
    }

    public function getGroup( $mix_fallback = null ) {

        return $this->getMaster( 'term_group', $mix_fallback );
    }


    public function getTaxonomyID( $mix_fallback = null ) {

        return $this->getMaster( 'term_taxonomy_id', $mix_fallback );
    }


    public function getTaxonomy( $mix_fallback = null ) {

        return $this->getMaster( 'taxonomy', $mix_fallback );
    }


    public function getDescription( $mix_fallback = null ) {

        return $this->getMaster( 'description', $mix_fallback );
    }


    public function getParent( $mix_fallback = null ) {

        return $this->getMaster( 'term_parent', $mix_fallback );
    }

    public function getCount( $mix_fallback = null ) {

        return $this->getMaster( 'count', $mix_fallback );
    }

    public function getFilter( $mix_fallback = null ) {

        return $this->getMaster( 'filter', $mix_fallback );
    }


    protected function getMaster( $str_prop = false, $mix_fallback = null ) {

        $str_prop = trim( $str_prop );
        if ( isset( $this->_obj_term->$str_prop ) ) {
            return $this->_obj_term->$str_prop;
        }
        if ( $mix_fallback !== null ) {
            return $mix_fallback;
        }

        return $this->_mix_ret;
    }


    /*
     * https://developer.wordpress.org/reference/functions/get_term_link/
     */
    protected function getTermLink() {

        if ( $this->_link === false ) {

            return $this->_link = 'TODO - Term - getTermLink()';
        }

        return $this->_link;
    }

    protected function getMeta() {

        if ( $this->_int_id === false ) {
            return false;
        }

        // do we already have meta?
        if ( $this->_obj_term_meta instanceof \WPezSuite\WPezAPI\Get\Meta\ClassMeta ) {
            return $this->_obj_term_meta;
        }

        $mix_get_term_meta = get_term_meta( $this->_int_id );

        if ( ! is_array( $mix_get_term_meta ) ) {
            $mix_get_term_meta = [];
        }

        $new                  = new ClassMeta();
        $bool                 = $new->setMeta( $mix_get_term_meta );
        $this->_obj_term_meta = $new;

        return $this->_obj_term_meta;
    }

}