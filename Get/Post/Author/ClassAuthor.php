<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Post\Author;

use WPezSuite\WPezAPI\Get\User\ClassUser;

class ClassAuthor extends ClassUser {

    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'url_posts':
            case 'posts_url':
                return $this->getAuthorPostsUrl();

            case 'newer_id':
            case 'next_id':
                return 'TODO - adjacent'; // <<<<<<<<<<<<<<<<

            case 'older_id':
            case 'prev_id':
                return 'TODO  - adjacent'; // <<<<<<<<<<<<<<<<<

            default:
                return $this->userSwitch( $str_prop );

        }
    }


    /**
     * TODO - clean up
     *
     * @param string $mix_fallback
     * @param string $author_nicename
     *
     * @return mixed|string
     */
    public function getAuthorPostsUrl( $mix_fallback = '', $author_nicename = '' ) {

        $author_base = 'author';

        $permalink_structure = get_option( 'permalink_structure' );
        $front               = substr( $permalink_structure, 0, strpos( $permalink_structure, '%' ) );

        $link = $author_structure = $front . $author_base . '/%author%';

        // return $author_structure;

        if ( empty( $link ) ) {
            $file = home_url( '/' );
            $link = $file . '?author=' . '$ auth_ID';
        } else {
            if ( '' == $author_nicename ) {
                $user = $this->_obj_user;
                if ( ! empty( $user->user_nicename ) ) {
                    $author_nicename = $user->user_nicename;
                }
            }
            $link = str_replace( '%author%', $author_nicename, $link );

            $use_trailing_slashes = ( '/' == substr( $this->permalink_structure, -1, 1 ) );
            if ( $use_trailing_slashes ) {
                $link = trailingslashit( $link );
            } else {
                $link = untrailingslashit( $link );
            }

            $link = home_url( $link );
        }

        return $link;

    }

}