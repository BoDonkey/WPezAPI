<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Post;

use WPezSuite\WPezAPI\Get\Post\Author\ClassAuthor;
use WPezSuite\WPezAPI\Get\Media\ClassMedia;
use WPezSuite\WPezAPI\Get\Meta\ClassMeta;
use WPezSuite\WPezAPI\Get\Post\Taxonomies\ClassTaxonomies;

class ClassPost {

    use \WPezSuite\WPezAPI\Get\Core\Traits\FilterTheContent\TraitFilterTheContent;

    protected $_bool_revision;
    protected $_int_id;
    protected $_obj_post;

    protected $_str_content_filtered;
    protected $_str_format;
    protected $_bool_sticky;
    protected $_str_template;
    protected $_str_permalink;
    protected $_str_shortlink;
    protected $_arr_post_class;

    protected $_bool_has_feat_img;
    protected $_int_feat_img_id;

    protected $_int_author_id;
    protected $_obj_author;

    protected $_int_img_id;
    protected $_obj_img;

    protected $_obj_meta;
    protected $_obj_taxs;

    public function __construct( $mix = false ) {

        $this->setPropertyDefaults();

        if ( $mix !== false ) {
            $this->setPostByID( $mix );
        }

    }


    protected function setPropertyDefaults() {

        $this->_bool_revision = false; // < TODO - set'er
        $this->_int_id        = false;
        $this->_obj_post      = false;

        $this->_str_content_filtered = false;
        $this->_str_format           = 'false';
        $this->_bool_sticky          = 'false';
        $this->_str_template         = false;
        $this->_str_permalink        = false;
        $this->_str_shortlink        = false;
        $this->_arr_post_class       = false;

        $this->_bool_has_feat_img = 'false';
        $this->_int_feat_img_id   = 'false';

        $this->_int_author_id = false;
        $this->_obj_author    = false;

        $this->_int_img_id = false;
        $this->_obj_img    = false;

        $this->_obj_meta = false;
        $this->_obj_taxs = false;
    }


    /**
     * TODO - revisit - there's a couple key TODOs now. Add $bool_active
     * property if the set / load isn't right then use $bool_active to return
     * false for property requests
     *
     * @param bool $mix
     *
     * @return bool
     */
    public function setPostByID( $mix = false ) {

        if ( $mix !== false ) {

            if ( $mix instanceof \WP_Post ) {

                $this->_int_id = absint( $mix->ID );
                $mix_get_post  = $mix;
            } else {

                $this->_int_id = absint( $mix );
                $mix_get_post  = get_post( $this->_int_id );
            }

            if ( $mix_get_post instanceof \WP_Post ) {
                $this->_obj_post      = $mix_get_post;
                $this->_int_author_id = $mix_get_post->post_author;

                // TODO - this should be delayed. It should only be done "on demand"
                $mix_get_post_thumbnail_id = get_post_thumbnail_id( $this->_int_id );

                if ( ! empty( $mix_get_post_thumbnail_id ) ) {
                    $this->_int_img_id = $mix_get_post_thumbnail_id;
                }

                return true;
            }
        }

        return false;
    }

    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            // core WP_Post properties

            // case 'ID':
            case 'id':
                return $this->getID();

            // Note: author (see below) will return a author (user) object
            case 'author_id':
            case 'post_author_id':
            case 'post_author':

                return $this->getAuthorID();

            case 'date':
            case 'post_date':
                return $this->getDate();

            case 'date_gmt':
            case 'post_date_gmt':
                return $this->getDateGMT();

            case 'content':
            case 'post_content':
                return $this->getContent();

            case 'content_filtered':
            case 'post_content_filtered':
                return $this->getPostContentFiltered();

            case 'title':
            case 'post_title':
                return $this->getTitle();

            case 'excerpt':
            case 'post_excerpt':
                return $this->getExcerpt();

            case 'status':
            case 'status_post':
            case 'post_status':
                return $this->getStatus();

            case 'status_comment':
            case 'comment_status':
                return $this->getStatusComment();

            case 'status_ping':
            case 'ping_status':
                return $this->getStatusPing();

            case 'password':
            case 'pwd':
            case 'post_password':
                return $this->getPassword();

            case 'slug':
            case 'name':
            case 'post_name':
                return $this->getSlug();

            case 'to_ping':
                return $this->getToPing();

            case 'pinged':
                return $this->getPinged();

            case 'modified':
            case 'post_modified':
                return $this->getModified();

            case 'modified_gmt':
            case 'post_modified_gmt':
                return $this->getModifiedGMT();

            case 'parent':
            case 'post_parent':
                return $this->getParent();

            case 'guid':
                // case 'GUID':
                return $this->getGUID();

            case 'order':
            case 'menu_order':
                return $this->getOrder();

            case 'type':
            case 'post_type':
                return $this->getType();

            case 'mime_type':
            case 'post_mime_type':
                return $this->getMimeType();

            case 'comment_count':
                return $this->getCommentCount();

            case 'filter':
                return $this->getFilter();

            // ---------

            case 'format':
            case 'post_format':
                return $this->getFormat();

            case 'sticky':
            case 'is_sticky':
                return $this->getSticky();

            case 'template':
                return $this->getTemplate();

            case 'feat_img_id':
            case 'featured_image_id':
            case 'thumbnail_id':
            case 'thumb_id':
            case 'post_thumbnail_id':
                return $this->getFeatImgID();

            case 'has_feat_img':
            case 'has_img':
            case 'has_image':
            case 'has_featured_image':
                return $this->hasFeatImg();

            case 'url':
            case 'link':
            case 'permalink':
                return $this->getURL();

            case 'url_short':
            case 'shortlink':
                return $this->getURLShort();

            case 'class':
            case 'post_class':
                return $this->getClass();

            case 'newer_id':
            case 'next_id':
                return 'TODO'; // <<<<<<<<<<<<<<<<

            case 'older_id':
            case 'prev_id':
                return 'TODO'; // <<<<<<<<<<<<<<<<<

            // --- objects ---

            case 'author':
                // case 'Author':
                return $this->getAuthor();

            case 'comments':
                // case 'Comments':
                return 'TODO';

            case 'image':
                //   case 'Image':
                return $this->getImage();

            case 'meta':
                //   case 'Meta':
                return $this->getMeta();

            case 'taxonomies':
                //    case 'Taxonomies':
                return $this->getTaxonomies();
        }

    }


    public function getID( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->ID ) ) {
            return $this->_obj_post->ID;
        }

        return $mix_fallback;
    }

    public function getAuthorID( $mix_fallback = '' ) {

        if ( isset( $this->_obj_post->post_author ) ) {
            return $this->_obj_post->post_author;
        }

        return $mix_fallback;
    }

    public function getDate( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_date ) ) {
            return $this->_obj_post->post_date;
        }

        return $mix_fallback;
    }

    public function getDateGMT( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_date_gmt ) ) {
            return $this->_obj_post->post_date_gmt;
        }

        return $mix_fallback;
    }


    public function getContent( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_content ) ) {
            return $this->_obj_post->post_content;
        }

        return $mix_fallback;
    }

    public function getPostContentFiltered( $mix_fallback = false ) {

        if ( $this->_str_content_filtered !== false ) {
            return $this->_str_content_filtered;
        }

        if ( isset( $this->_obj_post->post_content ) ) {
            $this->_str_content_filtered = $this->filterTheContent( $this->getContent( $mix_fallback ) );

            return $this->_str_content_filtered;
        }

        return $mix_fallback;
    }

    public function getTitle( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_title ) ) {
            return $this->_obj_post->post_title;
        }

        return $mix_fallback;
    }

    public function getExcerpt( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_excerpt ) ) {
            return $this->_obj_post->post_excerpt;
        }

        return $mix_fallback;
    }

    public function getStatus( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_status ) ) {
            return $this->_obj_post->post_status;
        }

        return $mix_fallback;
    }

    public function getStatusComment( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->comment_status ) ) {
            return $this->_obj_post->comment_status;
        }

        return $mix_fallback;
    }

    public function getStatusPing( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->ping_status ) ) {
            return $this->_obj_post->ping_status;
        }

        return $mix_fallback;
    }


    public function getPassword( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_password ) ) {
            return $this->_obj_post->post_password;
        }

        return $mix_fallback;
    }

    public function getSlug( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_name ) ) {
            return $this->_obj_post->post_name;
        }

        return $mix_fallback;
    }


    public function getToPing( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->to_ping ) ) {
            return $this->_obj_post->to_ping;
        }

        return $mix_fallback;
    }


    public function getPinged( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->pinged ) ) {
            return $this->_obj_post->pinged;
        }

        return $mix_fallback;
    }


    public function getModified( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_modified ) ) {
            return $this->_obj_post->post_modified;
        }

        return $mix_fallback;
    }

    public function getModifiedGMT( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_modified_gmt ) ) {
            return $this->_obj_post->post_modified_gmt;
        }

        return $mix_fallback;
    }

    public function getParent( $mix_fallback = 0 ) {

        if ( isset( $this->_obj_post->post_parent ) ) {
            return $this->_obj_post->post_parent;
        }

        return $mix_fallback;
    }


    public function getGUID( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->guid ) ) {
            return $this->_obj_post->guid;
        }

        return $mix_fallback;
    }

    public function getOrder( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->menu_order ) ) {
            return $this->_obj_post->menu_order;
        }

        return $mix_fallback;
    }

    public function getType( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_type ) ) {
            return $this->_obj_post->post_type;
        }

        return $mix_fallback;
    }

    public function getMimeType( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_mime_type ) ) {
            return $this->_obj_post->post_mime_type;
        }

        return $mix_fallback;
    }

    public function getCommentCount( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->comment_count ) ) {
            return $this->_obj_post->comment_count;
        }

        return $mix_fallback;
    }


    public function getFilter( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->filter ) ) {
            return $this->_obj_post->filter;
        }

        return $mix_fallback;
    }

    // --------------------------------------------

    public function getFormat() {

        if ( $this->_str_format != 'false' ) {
            return $this->_str_format;
        }
        $int_id            = $this->getID();
        $this->_str_format = get_post_format( $int_id );

        return $this->_str_format;
    }


    public function getSticky() {

        if ( $this->_bool_sticky != 'false' ) {
            return $this->_bool_sticky;
        }
        $int_id             = $this->getID();
        $this->_bool_sticky = is_sticky( $int_id );

        return $this->_bool_sticky;
    }


    public function getTemplate() {

        if ( $this->_str_template !== false ) {
            return $this->_str_template;
        }

        $int_id              = $this->getID();
        $this->_str_template = get_page_template_slug( $int_id );

        return $this->_str_template;
    }


    public function getFeatImgID() {

        if ( $this->_int_feat_img_id != 'false' ) {
            return $this->_int_feat_img_id;
        }

        $mix_id = get_post_thumbnail_id( $this->_obj_post->ID );
        if ( ! empty ( $mix_id ) ) {
            $this->_int_feat_img_id = absint( $mix_id );

            return $this->_int_feat_img_id;
        }
        $this->_int_feat_img_id = false;

        return $this->_int_feat_img_id;


    }


    public function hasFeatImg() {

        if ( is_bool( $this->_bool_has_feat_img ) ) {
            return $this->_bool_has_feat_img;
        }

        if ( $this->getFeatImgID() === false ) {
            $this->_bool_has_feat_img = false;

            return $this->_bool_has_feat_img;
        }
        $this->_bool_has_feat_img = true;

        return $this->_bool_has_feat_img;
    }

    /**
     * TODO - don't wait til add_action('setup_theme');
     *
     * @return mixed
     */
    public function getURL() {

        if ( $this->_str_permalink !== false ) {
            return $this->_str_permalink;
        }
        $int_id               = $this->getID();
        $this->_str_permalink = get_permalink( $int_id );

        return $this->_str_permalink;
    }

    public function getURLShort( $bool_allow_slugs = true ) {

        if ( $this->_str_shortlink !== false ) {
            return $this->_str_shortlink;
        }
        $int_id               = $this->getID();
        $this->_str_shortlink = wp_get_shortlink( $int_id, 'post', $bool_allow_slugs );

        return $this->_str_shortlink;
    }

    public function getClass( $mix_add = [] ) {

        if ( $this->_arr_post_class != false ) {
            return $this->_arr_post_class;
        }

        $ind_id                = $this->getID();
        $this->_arr_post_class = get_post_class( $mix_add, $ind_id );

        return $this->_arr_post_class;
    }


    // ----------------------------------------------


    protected function getAuthor() {

        // TODO instance of
        if ( $this->_obj_author instanceof \WPezSuite\WPezPost\Author\ClassAuthor ) {
            return $this->_obj_author;
        }

        if ( $this->_int_author_id !== false ) {

            $new               = new ClassAuthor();
            $bool              = $new->setUserByID( $this->_int_author_id );
            $this->_obj_author = $new;

            return $new;
        }

        return false;
    }

    protected function getImage() {

        if ( $this->_obj_img instanceof \WPezSuite\WPezAPI\Get\Media\ClassMedia ) {
            return $this->_obj_img;
        }

        if ( $this->_int_img_id !== false ) {

            $new            = new ClassMedia();
            $bool           = $new->setImageByImageID( $this->_int_img_id );
            $this->_obj_img = $new;

            return $new;
        }

        return false;
    }

    // TODO - make this a trait where the function ( get_post_meta) is an arg
    protected function getMeta() {

        if ( $this->_int_id === false ) {
            return false;
        }

        // do we already have the meta?
        if ( $this->_obj_meta instanceof \WPezSuite\WPezAPI\Get\Meta\ClassMeta ) {
            return $this->_obj_meta;
        }

        $mix_get_post_meta = get_post_meta( $this->_int_id );

        if ( ! is_array( $mix_get_post_meta ) ) {
            $mix_get_user_meta = [];
        }

        $new  = new ClassMeta();
        $bool = $new->setMeta( $mix_get_post_meta );
        // TODO test $bool
        $this->_obj_meta = $new;

        return $this->_obj_meta;
    }

    protected function getTaxonomies() {

        if ( $this->_obj_taxs instanceof \WPezSuite\WPezAPI\Get\Post\Taxonomies\ClassTaxonomies ) {
            return $this->_obj_taxs;
        }

        if ( $this->_obj_post instanceof \WP_Post ) {

            $new = new ClassTaxonomies();
            $new->setPostObject( $this->_obj_post );
            $this->_obj_taxs = $new;

            return $new;
        }

        return false;
    }


}