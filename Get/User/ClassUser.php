<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\User;

use WPezSuite\WPezAPI\Get\Meta\ClassMeta;


class ClassUser {

    protected $_mix_ret;

    protected $_int_id;
    protected $_obj_user;

    protected $_obj_user_data;
    protected $_obj_user_meta;

    public function __construct() {

        $this->setPropertyDefaults();

    }


    protected function setPropertyDefaults() {

        $this->_mix_ret       = false;
        $this->_int_id        = false;
        $this->_obj_user      = false;
        $this->_obj_user_data = false;
        $this->_obj_user_meta = false;

    }

    public function setUserByID( $mix = false ) {

        if ( $mix !== false ) {

            if ( $mix instanceof \WP_User ) {

                $int_id = absint( $mix->ID );
            } else {

                $int_id = absint( $mix );
            }

            return $this->getUserByField( 'id', $int_id );
        }

        return false;
    }

    public function setUserByNiceName( $mix = false ) {

        return $this->setUserBySlug( $mix );
    }

    public function setUserBySlug( $mix = false ) {

        if ( $mix !== false ) {

            if ( $mix instanceof \WP_User ) {

                $str_slug = $mix->user_nicename;
            } else {

                $str_slug = (string)$mix;
            }

            return $this->getUserByField( 'slug', $str_slug );
        }

        return false;
    }

    public function setUserByEmail( $mix = false ) {

        if ( $mix !== false ) {

            if ( $mix instanceof \WP_User ) {

                $str_email = $mix->user_email;
            } else {

                $str_email = (string)$mix;
            }

            return $this->getUserByField( 'email', $str_email );
        }

        return false;
    }

    public function setUserByLogin( $mix = false ) {

        if ( $mix !== false ) {

            if ( $mix instanceof \WP_User ) {

                $str_login = $mix->user_login;
            } else {

                $str_login = (string)$mix;
            }

            return $this->getUserByField( 'login', $str_login );
        }

        return false;
    }


    protected function getUserByField( $str_field, $mix_value ) {

        // let's start with a clean slate
        $this->setPropertyDefaults();

        $mix_get_user_by = $this->getUserBy( $str_field, $mix_value );

        if ( $mix_get_user_by instanceof \WP_User ) {

            $this->_int_id   = $mix_get_user_by->ID;
            $this->_obj_user = $mix_get_user_by;

            //  var_dump( $mix_get_user_by );

            if ( ! isset( $this->_obj_user->data ) ) {
                return false;
            }
            // we have this from the WP_User, let's add it now.
            $this->_obj_user_data = $this->_obj_user->data;

            return true;
        }

        return false;
    }

    /**
     * Because get_user_by() is plugable:
     * https://wordpress.stackexchange.com/questions/134796/get-userdata-inside-custom-build-plugin
     *
     * ref: https://developer.wordpress.org/reference/functions/get_user_by/
     *
     * @param $field
     * @param $value
     *
     * @return bool|\WP_User
     */
    protected function getUserBy( $field, $value ) {

        $userdata = \WP_User::get_data_by( $field, $value );

        if ( ! $userdata ) {
            return false;
        }

        $user = new \WP_User();
        $user->init( $userdata );

        return $user;
    }

    // https://codex.wordpress.org/Function_Reference/the_author_meta
    public function __get( $str_prop ) {

        return $this->userSwitch( ( $str_prop ) );
    }



    protected function userSwitch( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'id':
                // case 'ID':
                return $this->getID();

            case 'url_avatar':
            case 'url_gravatar':
                return $this->getGravatarUrl();

            case 'desc':
            case 'bio':
            case 'bio_info':
            case 'bio_desc':
            case 'description':
                return $this->getMeta()->description;

            case 'name_display':
            case 'display_name':
                return $this->_obj_user_data->display_name;

            case 'name_first':
            case 'first_name':
                return $this->getMeta()->first_name;

            case 'name_last':
            case 'last_name':
                return $this->getMeta()->last_name;

            case 'name_nice':
            case 'nicename':
            case 'nice_name':
            case 'namenice':
            case 'user_nicename':
                // case 'NiceName':
                //  case 'NameNice':
                return $this->getNameNice();

            case 'meta':
                //   case 'Meta':
                return $this->getMeta();

            case 'data':
                // case 'Data':
                return $this->_obj_user_data;

            case 'url':
                return 'TODO - url to user profile. __not__ the same as url_posts';

            case 'slug':
                return 'TODO';

            default:
                return $this->_mix_ret;
        }
    }


    public function getID( $mix_fallback = null ) {

        if ( isset( $this->_obj_user->ID ) ) {
            return $this->_obj_user->ID;
        }

        if ( $mix_fallback !== null ) {
            return $mix_fallback;
        }

        return $this->_mix_ret;

    }


    // TODO allow adding query string args
    public function getGravatarUrl( $mix_fallback = '', $arr_qs = [] ) {

        if ( isset( $this->_obj_user_data->user_email ) ) {

            $email      = $this->_obj_user_data->user_email;
            $email_hash = md5( strtolower( trim( $email ) ) );

            $str_url = 'https://secure.gravatar.com/avatar/' . $email_hash;

            // TODO - what if nothing comes back?
            return $str_url;
        }

        return $mix_fallback;
    }

    protected function getMeta() {

        if ( $this->_int_id === false ) {
            return false;
        }

        // do we already have meta?
        if ( $this->_obj_user_meta instanceof \WPezSuite\WPezPost\Core\Meta\ClassMeta ) {
            return $this->_obj_user_meta;
        }

        $mix_get_user_meta = get_user_meta( $this->_int_id );

        if ( ! is_array( $mix_get_user_meta ) ) {
            $mix_get_user_meta = [];
        }

        $new                  = new ClassMeta();
        $bool                 = $new->setMeta( $mix_get_user_meta );
        $this->_obj_user_meta = $new;

        return $this->_obj_user_meta;
    }


    public function getNiceName( $mix_fallback = '' ) {

        return $this->getNameNice( $mix_fallback );
    }

    public function getNameNice( $mix_fallback = '' ) {

        if ( isset( $this->_obj_user_data->user_nicename ) ) {
            return $this->_obj_user_data->user_nicename;
        }

        return $mix_fallback;
    }


}