<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Media\Image;

class ClassImage {

    protected $_int_id;
    protected $_str_size;
    protected $_str_file;
    protected $_int_width;
    protected $_int_height;
    protected $_str_mine_type;
    protected $_bool_icon;
    protected $_str_path;
    protected $_int_filesize;
    protected $_arr_filesize_human;

    protected $_str_url;
    protected $_bool_is_intermediate;
    protected $_str_srcset;

    public function __construct() {

        $this->setPropertyDefaults();
    }


    protected function setPropertyDefaults() {

        $this->_int_id             = false;
        $this->_str_size           = '';
        $this->_str_file           = false;
        $this->_int_width          = 0;
        $this->_int_height         = 0;
        $this->_str_mine_type      = false;
        $this->_bool_icon          = false;
        $this->_str_path           = false;
        $this->_int_filesize       = false;
        $this->_arr_filesize_human = false;

        $this->_str_url              = false;
        $this->_bool_is_intermediate = 'false';
        $this->_str_srcset           = false;
    }


    public function setID( $int_id = false ) {

        $this->_int_id = absint( $int_id );
    }

    public function setSize( $str_size = false ) {

        $this->_str_size = $str_size;
    }

    public function setFile( $str_file = false ) {

        $this->_str_file = $str_file;
    }

    public function setWidth( $int_w = false ) {

        $this->_int_width = absint( $int_w );
    }

    public function setHeight( $int_h = false ) {

        $this->_int_height = absint( $int_h );
    }

    public function setMimeType( $str_mt = false ) {

        $this->_str_mine_type = $str_mt;
    }


    public function setIcon( $bool = false ) {

        $this->_bool_icon = (boolean)$bool;
    }

    public function setPath( $str = false ) {

        $this->_str_path = $str;
    }


    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'id':
                //  case 'ID':

                // TODO get
                return $this->_int_id;

            case 'width':
            case 'w':
            case 'wide':
                // TODO get
                return $this->_int_width;

            case 'height':
            case 'h':
            case 'high':
                // TODO get
                return $this->_int_height;

            case 'path':
                // TODO get
                return $this->_str_path;

            case 'file':
                // TODO get
                return $this->_str_file;

            case 'filesize':
                return $this->getFilesize();

            case 'filesize_human':
                return $this->getFilesizeHuman();

            case 'size':
                // TODO get
                return $this->_str_size;

            case 'srcset':
                return $this->getAttachmentImageSrcset();

            case 'intermediate':
            case 'is_intermediate':
                return $this->getIsIntermediate();

            case 'url':
                return $this->getURL();
        }
    }


    protected function getURL( $mix_ret = '' ) {

        if ( $this->_str_url !== false ) {
            return $this->_str_url;
        }
        $this->getAttachmentImageSrc();

        return $this->_str_url;
    }

    protected function getIsIntermediate( $mix_ret = 'false' ) {

        if ( $this->_bool_is_intermediate !== 'false' ) {
            return $this->_bool_is_intermediate;
        }
        $this->getAttachmentImageSrc();

        return $this->_bool_is_intermediate;
    }

    // https://developer.wordpress.org/reference/functions/wp_get_attachment_image_src/
    public function getAttachmentImageSrc( $TODO_arg1 = '', $TODO_arg2 = '' ) {

        $arr = wp_get_attachment_image_src( $this->_int_id, $this->_str_size, $this->_bool_icon );

        $this->_str_url              = $arr[0];
        $this->_bool_is_intermediate = $arr[3];

    }

    // https://developer.wordpress.org/reference/functions/wp_get_attachment_image_srcset/
    public function getAttachmentImageSrcset() {

        if ( $this->_str_srcset !== false ) {
            return $this->_str_srcset;
        }
        // TODO array $image_meta = null
        $this->_str_srcset = wp_get_attachment_image_srcset( $this->_int_id, $this->_str_size );

        return $this->_str_srcset;
    }

    // TODO build your own scrset. arg = arr of sizes. (does this belonw in this class?)


    public function getFilesize() {

        if ( $this->_int_filesize !== false ) {
            return $this->_int_filesize;
        }
        if ( $this->_str_path === false ) {
            return false;
        }
        if ( file_exists( $this->_str_path . $this->_str_file ) ) {
            $this->_int_filesize = filesize( $this->_str_path . $this->_str_file );

            return $this->_int_filesize;
        }

        return false;
    }

    public function getFilesizeHuman() {

        if ( $this->_arr_filesize_human !== false ) {
            return $this->_arr_filesize_human;
        }
        $int_temp = $this->getFilesize();

        $arr_defaults = [
            'B'  => false,
            'GB' => false,
            'KB' => false,
            'MB' => false,
            'TB' => false
        ];

        if ( $int_temp === false ) {
            return $arr_defaults;
        }


        $arr_temp                  = $this->filesizeHuman( $int_temp );
        $this->_arr_filesize_human = array_merge( $arr_defaults, $this->filesizeHuman( $int_temp ) );

        return $this->_arr_filesize_human;
    }


    /**
     * Source: https://secure.php.net/manual/en/function.filesize.php
     *
     * @param $int_bytes
     *
     * @return array
     */
    protected function filesizeHuman( $int_bytes ) {

        $int_bytes  = floatval( $int_bytes );
        $arr_args   = [];
        $arr_args[] = [
            'U' => "TB",
            'V' => pow( 1024, 4 )
        ];
        $arr_args[] = [
            'U' => "GB",
            'V' => pow( 1024, 3 )
        ];

        $arr_args[] = [
            'U' => "MB",
            'V' => pow( 1024, 2 )
        ];
        $arr_args[] = [
            'U' => "KB",
            'V' => 1024
        ];
        $arr_args[] = [
            'U' => "B",
            'V' => 1
        ];

        $arr_ret = [];
        foreach ( $arr_args as $arr ) {
            $arr_ret[ $arr['U'] ] = $int_bytes / $arr['V'];

        }

        return $arr_ret;
    }
}