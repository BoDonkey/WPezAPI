<?php
/**
 * Created by PhpStorm.
 */

namespace WPezSuite\WPezAPI\Get\Media;

use WPezSuite\WPezAPI\Get\Meta\ClassMeta;
use WPezSuite\WPezAPI\Get\Media\Image\ClassImage;


class ClassMedia {

    use \WPezSuite\WPezAPI\Get\Core\Traits\FilterTheContent\TraitFilterTheContent;

    protected $_int_id;
    protected $_obj_img;
    protected $_arr_sizes_registered;

    protected $_str_desc_filtered;
    protected $_str_file;
    protected $_str_path;

    protected $_bool_has_feat_img;
    protected $_int_feat_img_id;

    protected $_arr_sizes;
    protected $_str_size;
    protected $_arr_get_size;

    protected $_obj_img_meta;

    public function __construct( $TODO = true ) {

        $this->setPropertyDefaults();
    }


    protected function setPropertyDefaults() {

        $this->_int_id               = false;
        $this->_obj_img              = false;
        $this->_arr_sizes_registered = false;

        $this->_str_desc_filtered = false;
        $this->_str_file          = false;
        $this->_str_path          = false;
        $this->_bool_has_feat_img = 'false';
        $this->_int_feat_img_id   = 'false';


        $this->_arr_sizes    = false;
        $this->_str_size     = false;
        $this->_arr_get_size = false;
        $this->_obj_img_meta = false;
    }


    public function setImageByPostID( $int_id = false ) {

        $int_id = absint( $int_id );
        $mix    = get_post_thumbnail_id( $int_id );

        // TODO - post_type == 'attachment'
        if ( ! empty( $mix ) ) {
            $this->_int_id = $mix;
        }


    }

    // TODO - wp_attachment_is_image()
    public function setImageByImageID( $int_id = false ) {

        $this->_int_id = $int_id;
        // TODO check to see if it's a media post
    }


    // TOTO wp_prepare_attachment_for_js()
    public function __get( $str_prop ) {

        $str_prop = strtolower( $str_prop );

        switch ( $str_prop ) {

            case 'id':
                return $this->_int_id;

            case 'class':

                return 'TODO';

            case 'alt':
            case 'alt_text':
            case 'alternative_text':
                return $this->getAlt();

            case 'caption':
            case 'excerpt':
            case 'post_excerpt':
                return $this->getCaption();

            case 'desc':
            case 'description':
            case 'content':
            case 'post_content':
                return $this->getDesc();

            case 'desc_filtered':
            case 'description_filtered':
            case 'content_filtered':
            case 'post_content_filtered':
                return $this->getDescFiltered();

            case 'slug':
            case 'name':
            case 'post_name':
                return $this->getSlug();

            case 'mime_type':
            case 'post_mime_type':
                return $this->getMimeType();

            case 'parent':
                return 'TODO';

            case 'sizes':
                // case 'Sizes':
                return $this->getSizes();

            case 'meta':
                // case 'Meta';
                return $this->getMeta();

            // as based on size set by setSize()
            case 'size':
                return $this->getSize();


            // -----------------------
            case 'file':
                return $this->getFile();

            case 'path':
                return $this->getPath();

            // https://developer.wordpress.org/reference/functions/wp_attachment_is/
            case 'is':
            case 'attachment_is':
                return 'TODO';


            // https://codex.wordpress.org/Function_Reference/get_children
            // https://developer.wordpress.org/reference/functions/get_children/
            case 'attachments':
                return 'TODO';

            case 'children':
                return 'TODO';

            // -------------------------

            default:
                /**
                 * you can access a given size directly BUT __only if__:
                 *
                 * 1) that size name is not a "property" name (else it'll never make it this far.
                 * 2) the size names use _ and not - (or any char that prevents it from being treated like a "property"
                 *
                 * else use the method: getSize('some-size')
                 */

                $arr_sizes = $this->getSizes();
                if ( isset( $arr_sizes[ $str_prop ] ) ) {
                    return $this->getSize( $str_prop );
                }

                return '';
        }


    }


    public function getFile( $mix_fallback = false ) {

        if ( $this->_str_file !== false ) {
            return $this->_str_file;
        }

        $mix_temp = get_attached_file( $this->_int_id );
        if ( $mix_temp === false ) {
            return $mix_fallback;
        }

        $this->_str_file = $mix_temp;


        return $this->_str_file;

    }

    public function getPath( $mix_fallback = false ) {

        if ( $this->_str_path !== false ) {
            return $this->_str_path;
        }

        $mix_temp = $this->getFile();
        if ( $mix_temp !== false ) {

            $this->_str_path = dirname( $mix_temp );

            return $this->_str_path;
        }

        return $mix_fallback;
    }

    public function getRegisteredSizes() {

        if ( $this->_arr_sizes_registered !== false ) {
            return $this->_arr_sizes_registered;
        }

        return get_intermediate_image_sizes();

    }


    public function getSlug( $mix_fallback = false ) {

        if ( isset( $this->_obj_post->post_name ) ) {
            return $this->_obj_post->post_name;
        }

        return $mix_fallback;
    }


    public function getMimeType( $mix_fallback = '' ) {

        if ( isset( $this->getPost()->post_mime_type ) ) {
            return $this->getPost()->post_mime_type;
        }

        return $mix_fallback;
    }

    public function getAlt( $mix_fallback = '' ) {

        $str_alt = $this->getMeta()->_wp_attachment_image_alt;
        if ( ! empty( $str_alt ) ) {
            return $str_alt;
        }

        return $mix_fallback;

    }

    public function getCaption( $mix_fallback = '' ) {

        if ( isset( $this->getPost()->post_excerpt ) ) {
            return $this->getPost()->post_excerpt;
        }

        return $mix_fallback;
    }

    // TODO compare w/ post class
    public function getDescFiltered( $mix_fallback = '' ) {

        if ( $this->_str_desc_filtered !== false ) {
            return $this->_str_desc_filtered;

        }
        $this->_str_desc_filtered = $this->filterTheContent( $this->getDesc() );

        return $this->_str_desc_filtered;;
    }


    public function getDesc( $mix_fallback = '' ) {

        if ( isset( $this->getPost()->post_content ) ) {
            return $this->getPost()->post_content;
        }

        return $mix_fallback;
    }

    protected function getPost() {

        if ( $this->_obj_img instanceof \WP_Post ) {
            return $this->_obj_img;
        }

        $mix_temp = get_post( $this->_int_id );
        if ( $mix_temp !== false && $mix_temp->post_type == 'attachment'){
            $this->_obj_img = $mix_temp;
            return $this->_obj_img;
        }

        return false;
    }

    protected function getMeta() {

        if ( $this->_int_id === false ) {
            return false;
        }

        // do we already have meta?
        if ( $this->_obj_img_meta instanceof \WPezSuite\WPezAPI\Get\Meta\ClassMeta ) {
            return $this->_obj_img_meta;
        }

        $mix_get_post_meta = get_post_meta( $this->_int_id );

        if ( ! is_array( $mix_get_post_meta ) ) {
            $mix_get_post_meta = [];
        }

        $new                 = new ClassMeta();
        $bool                = $new->setMeta( $mix_get_post_meta );
        $this->_obj_img_meta = $new;

        return $this->_obj_img_meta;
    }


    // TODO - add orginal - treat it light any other size
    public function getSizes() {

        if ( is_array( $this->_arr_sizes ) ) {
            return $this->_arr_sizes;
        }

        $arr_att_meta = $this->getMeta()->_wp_attachment_metadata;

        if ( isset( $arr_att_meta['sizes'] ) && is_array( $arr_att_meta['sizes'] ) ) {
            $this->_arr_sizes = $arr_att_meta['sizes'];
            foreach ( $this->_arr_sizes as $key => $arr_size ) {
                $arr_size['mime_type'] = $arr_size['mime-type'];
                unset( $arr_size['mime-type'] );
                $this->_arr_sizes[ $key ] = (object)$arr_size;
            }

            return $this->_arr_sizes;
        }

        return [];
    }


    public function setSize( $str_size = false ) {

        $this->_str_size = $str_size;
    }


    // TODO $mix_size can also be an array
    public function getSize( $mix_size = false, $bool_icon = false ) {

        if ( $mix_size === false && $this->_str_size !== false ) {

            $str_size = $this->_str_size;
        } else {

            $str_size = $mix_size;
        }

        if ( isset( $this->_arr_get_size[ $mix_size ] ) && $this->_arr_get_size[ $mix_size ] instanceof \WPezSuite\WPezAPI\Get\Media\Image\ClassImage ) {

            return $this->_arr_get_size[ $mix_size ];
        }

        $arr_sizes = $this->getSizes();
        if ( isset( $arr_sizes[ $str_size ] ) && is_object( $arr_sizes[ $str_size ] ) ) {


            $new = new ClassImage();
            $new->setID( $this->_int_id );
            $new->setFile( $arr_sizes[ $str_size ]->file );
            $new->setWidth( $arr_sizes[ $str_size ]->width );
            $new->setHeight( $arr_sizes[ $str_size ]->height );
            $new->setMimeType( $arr_sizes[ $str_size ]->mime_type );
            $new->setSize( $mix_size );
            $new->setIcon( $bool_icon );

            $str_path = $this->getPath() . DIRECTORY_SEPARATOR;
            $new->setPath( $str_path );

            $this->_arr_get_size[ $mix_size ] = $new;

            return $new;


        }

        return (object)[];

    }


}
