## WPezAPI

__A collection of WP classes and functions remixed The ezWay.__

Note: The below is focused on the Get components (as nothing else has been addressed yet).


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

There are a couple essential things to know about how this (i.e., Get) works (and why it's so wonderful):

1) Many frequently used functions (i.e., the value they return) are now accessible as object properties (via a magic __get() method). 

   For example, get_permalink() becomes $new_post->url or $new_post->getURL(),_with $new_post being an instance of the WPezAPI\Get\Post object_.

2) Traditional WordPress classes and functions are no longer "ordered randomly." Instead there's a logical and more natural hierarchy. 

   For example, the Post object has a property ->author. It's an Author object, and that Author class extends our User class. The result? Now you retrieve the post author's display name with: $new_post->Author->name_display or $new_post->Author->display_name.

3) What's even better is...when appropriate, property values are added to an object "on demand." That is, for example, the get_permalink() function isn't run until you request the ->url (property), or the Post's Image object won't be created until you request an Image property.

4) That said, when you start with an instance of WPezAPI\Get\Post it will (automatically) instantiate any subsequent "children" (e.g., Taxonomies) as needed. You don't have to worry about "manually" instantiating anything further, and nothing will be created until you actually need it. 

5) It is strongly recommended you dip into the code of each class to get a better sense of what's available to you (and what's still a TODO). Look for the magic __get(). 

6) Please see the Examples section below.


### SETUP

1) Add a folder named WPezSuite to your wp-content folder. That is, this folder will be a sibling to the folders: plugins , themes, etc.

2) Place WPezAPI under the WPezSuite folder. Ref: https://codex.wordpress.org/Must_Use_Plugins

3) Add a file to mu-plugins that requires (or includes) WPezAPI/Get's wpez-get.php. This file boots up the autoloader.

3) That's it. Now instantiate...

$new = new \WPezSuite\WPezAPI\Get\Post\ClassPost( "some post ID" );


### EXAMPLES

__> Post title e.g., $post->post_title__

$new->title

__> Post Meta (all)__

$new->Meta

_Note: Properties are not case sensitive. However, for readability / maintainability, using upper case for properties that are actually classes is strongly recommended._


__> Post Meta with a key of 'some_meta_key'__

$new->Meta->some_meta_key 

_Note: For the Meta class, by default, the __get() is single = true. Also, if your meta keys use - instead of _ this isn't going to work. But you can do..._

__> Post Meta with a key of 'some-meta-key'__

$new->Meta->getSingle('some-meta-key')

__> Post Meta with a key of 'some_other_meta_key' where there are multiple (meta) rows for this key__

$new->Meta->getMutli('some_other_meta_key');

__> Post Author Meta (all)__

$new->Author->Meta

__ Post Author > Meta > last_name

$new->Author->Meta->last_name

__> Post Taxonomies__

$new->Taxonomies

_Note: This is not all the taxonomies, only those attached to this particular post._

__> Post Taxonomies > the taxonomy: post_tag__

$new->Taxonomies->Post_Tag

__> Post Taxonomies > the taxonomy: post_tag > the term: tag_1__

$new->Taxonomies->Post_Tag->Term_1

_Note: This will return our own version of a Terms class (that is more complete than WP_Term)._


That's the gist of it. You can experiment with Post->Author, Post->Image, etc.  


> --
>
> If you have any questions, comments, or suggestions, please open an Issue. 
>
> --





### FAQ

__1 - Why?__

Too much thinking of (WP) data / models as a series of functions got the best of me :) 

__2 - Is this a REST API?__

No. It is not. Nor it is intended to be. That said, when it made sense we try to mirror the REST API's naming convention. For example, ->post_title is not simply ->title (since we already know we're working with the Post object).



### HELPFUL LINKS

- TODO
 
 
### TODO

- Overview, Setup, more / better Examples

- Use WP_Error?

- Post \ Adjacent

- Comments and (perhaps) Post \ Comments

- Settings

- Taxonomies & Taxonomies \ Terms (not to be confused with what's under Post)

- Other CRUD (as opposed to only GET)

### CHANGE LOG

__-- 0.0.1 (28 May 2018)__

- Various refactoring

__-- 0.0.1__

- INIT - And away we go...


